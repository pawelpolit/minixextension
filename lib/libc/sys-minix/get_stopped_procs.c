#include <lib.h>
#include <minix/minlib.h>
#include <errno.h>

int get_stopped_procs(pid_t* buf) {
	
	message m;
	int num;

	m.m1_p1 = (char*)buf;

	num = _syscall(PM_PROC_NR, GETSTOPPEDPROCS, &m);
	
	if(num == -1) {
		errno = EINVAL;
		return -1;
	}

	return num;
}
